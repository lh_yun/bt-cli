# 介绍

宝塔 CLI 是一个基于 node 进行命令行对宝塔面板api操作的工具，提供：

- 默认配置 地址和端口 127.0.0.1:8888,需要传入密钥，地址和端口可自定义传入。
- 封装对JAVA项目进行列表查看（查找），启动、停止的命令行工具。
- 并提供一个自定义请求工具，可传入接口和参数。

git 地址：https://gitee.com/lh_yun/bt-cli

npm 地址：https://www.npmjs.com/package/@id_yun/bt-cli

欢迎大家来使用或提交

### 说明

宝塔API接口使用教程 https://www.bt.cn/bbs/thread-20376-1-1.html

参考上面几位大佬，因为用到 TeamCity cli持续集成工具来打包发布项目，但是宝塔没有提供cli 工具，所以自己写了个启动和停止项目的node工具。

windows bat 的 java 启动停止例子

[start.bat](https://gitee.com/lh_yun/bt-cli/blob/master/start.bat)

[stop.bat](https://gitee.com/lh_yun/bt-cli/blob/master/stop.bat)
## 开始

安装:

```bash
npm install -g @id_yun/bt-cli
# OR
yarn global add @id_yun/bt-cli
```
宝塔开启AP接口:

![RUNOOB 图标](https://gitee.com/lh_yun/bt-cli/blob/master/%E5%AE%9D%E5%A1%94%E5%BC%80%E5%90%AFAP%E6%8E%A5%E5%8F%A3.png)

使用命令:

### 查看帮助 `--help`

```bash
bt-cli --help
```

```
Usage: bt-cli [options] [command] <url> [data]

宝塔CLI node Api工具，必须在开启API接口和配置IP白名单，Api的配置：地址、端口、密钥。
宝塔API接口使用教程 https://www.bt.cn/bbs/thread-20376-1-1.html

Arguments:
  url                  URL接口,值 /system?action=GetSystemTotal，例 bt-cli -k 接口密钥 /system?action=GetSystemTotal
  data                 URL参数,有特殊符号需要用双引号包裹""，例 "&"

Options:
  -V, --version        输出版本号
  -h, --host <host>    宝塔地址 (default: "127.0.0.1")
  -P, --port <number>  宝塔端口号 (default: 8888)
  --https              使用 https (default: false)
  -k, --key <key>      宝塔密钥,必填
  --help               display help for command

Commands:
  java [options]       JAVA 项目的列表(list)、启动(start)、停止(stop)

```

### java 项目帮助`java --help`

```bash
bt-cli java --help
```

```
Usage: bt-cli java [options]

JAVA 项目的列表(list)、启动(start)、停止(stop)

Options:
  -L, --list [projectName]   项目列表,默认查询10条,例 bt-cli -k API的KEY java -L
  -S, --start <projectName>  启动项目,例 bt-cli -k API的KEY java -S
  -K, --stop <projectName>   停止项目,例 bt-cli -k API的KEY java -K
  -h, --help                 display help for command
```

### JAVA 项目的列表,默认查询10条`java -L`, `ruoyi`为搜索项目名可以不填写

`-k` 密钥参数必须填写,`-h` 地址可以不填写，`-P`端口可以不填写

```bash
bt-cli -h 192.168.0.1 -P 8888 -k CoX7CB8fLit34BrbMP9ThYiWoB3Nb2w7 java -L ruoyi
# OR
bt-cli -k CoX7CB8fLit34BrbMP9ThYiWoB3Nb2w7 java -L
# OR
bt-cli -k CoX7CB8fLit34BrbMP9ThYiWoB3Nb2w7 java --list
```

```
{
  "page": "",
  "shift": "0",
  "row": "1",
  "data": [
    {
      "id": 1,
      "name": "ruoyi-gateway",
      "path": "D:/wwwroot/jar/ruoyi-gateway/target/ruoyi-gateway.jar",
      "status": "1",
      "index": null,
      "ps": "ruoyi-gateway",
      "addtime": "2022-06-29 21:58:50",
      "edate": "0000-00-00",
      "type_id": 0,
      "type_version": "",
      "project_type": "Java",
      "project_config": {
        "ssl_path": "D:/wwwroot/java_node_ssl",
        "project_name": "ruoyi-gateway",
        "project_jar": "D:/wwwroot/jar/ruoyi-gateway/target/ruoyi-gateway.jar",
        "bind_extranet": 1,
        "domains": [
          "182.246.32.67:80"
        ],
        "run_user": "springboot",
        "port": 8080,
        "auth": 0,
        "project_cmd": "D:/BtSoft/jdk/jdk1.8/bin/java.exe -jar -Dfile.encoding=utf-8 -Xmx1024M -Xms256M  D:/wwwroot/jar/ruoyi-gateway/target/ruoyi-gateway.jar --server.port=8080",
        "java_type": "springboot",
        "jar_path": "D:/wwwroot/jar/ruoyi-gateway/target",
        "pids": "D:/BtSoft/springboot/vhost/pids/ruoyi-gateway.pid",
        "logs": "D:/BtSoft/springboot/vhost/logs/ruoyi-gateway.log",
        "scripts": "D:/BtSoft/springboot/vhost/scripts/ruoyi-gateway.py",
        "is_separation": 1,
        "static_path": "D:/wwwroot/html",
        "api_url": "/api",
        "host_url": "http://127.0.0.1:8080",
        "project_jdk": "D:/BtSoft/jdk/jdk1.8/bin/java.exe"
      },
      "type": "PHP",
      "run": false,
      "load_info": {},
      "ssl": -1,
      "listen": [],
      "listen_ok": true,
      "debug": false
    }
  ]
}
```

### JAVA 启动项目`java -S`, `ruoyi-gateway`为项目全名必须填写

`-k` 密钥参数必须填写,`-h` 地址可以不填写，`-P`端口可以不填写

```bash
bt-cli -h 192.168.0.1 -P 8888 -k CoX7CB8fLit34BrbMP9ThYiWoB3Nb2w7 java -S ruoyi-gateway
# OR
bt-cli -k CoX7CB8fLit34BrbMP9ThYiWoB3Nb2w7 java -S ruoyi-gateway
# OR
bt-cli -k CoX7CB8fLit34BrbMP9ThYiWoB3Nb2w7 java --start ruoyi-gateway
```

```
{"status":true,"msg":"启动成功"}
```

### JAVA 停止项目`java -K`, `ruoyi-gateway`为项目全名必须填写

`-k` 密钥参数必须填写,`-h` 地址可以不填写，`-P`端口可以不填写

```bash
bt-cli -h 192.168.0.1 -P 8888 -k CoX7CB8fLit34BrbMP9ThYiWoB3Nb2w7 java -K ruoyi-gateway
# OR
bt-cli -k CoX7CB8fLit34BrbMP9ThYiWoB3Nb2w7 java -K ruoyi-gateway
# OR
bt-cli -k CoX7CB8fLit34BrbMP9ThYiWoB3Nb2w7 java --stop ruoyi-gateway
```

```
{"status":true,"msg":"停止成功"}
```

自定义接口请求 `url data`, url接口必须填写，data参数可不填写

`-k` 密钥参数必须填写,`-h` 地址可以不填写，`-P`端口可以不填写

### 比如获取系统基础统计

接口：`/system?action=GetSystemTotal`

参数：无

```bash
bt-cli -h 192.168.0.1 -P 8888 -k CoX7CB8fLit34BrbMP9ThYiWoB3Nb2w7 /system?action=GetSystemTotal
```

```
{
  "memTotal": 16383,
  "memFree": 10845,
  "memRealUsed": 5537,
  "cpuNum": 8,
  "cpuRealUsed": 0.31,
  "time": "2天19小时14分钟",
  "system": "Windows Server 2016 Standard (build 14393) x64 (Py3.8.6)",
  "isuser": 0,
  "version": "7.6.0"
}
```

### 比如软件商店搜索 nginx

接口 `/plugin?action=get_soft_list`

参数 `"p=1&type=-1&tojs=soft.get_list&force=0&query=nginx"`

```bash
bt-cli -h 192.168.0.1 -P 8888 -k CoX7CB8fLit34BrbMP9ThYiWoB3Nb2w7 /plugin?action=get_soft_list "p=1&type=-1&tojs=soft.get_list&force=0&query=nginx"
```

```
{
  "type": [],
  "list": {
    "page": "",
    "data": [
      {
        "id": 87,
        "os": "Windows",
        "pid": 100000019,
        "type": 8,
        "sort": 993,
        "price": 39.8,
        "name": "total_nginx",
        "title": "Nginx网站监控报表",
        "title_en": "",
        "panel_pro": 1,
        "panel_ltd": 1,
        "panel_free": 1,
        "panel_test": 1,
        "panel_en": 0,
        "ps": "快速分析网站运行状况，实时精确统计网站流量、ip、uv、pv、请求、蜘蛛等数据，网站SEO优化利器",
        "ps_en": "",
        "version": "",
        "s_version": "0",
        "manager_version": "1",
        "c_manager_version": "1",
        "dependnet": "nginx",
        "mutex": "",
        "install_checks": "D:/BtSoft/panel/plugin/total_nginx/total_nginx_main.pyd",
        "uninsatll_checks": "D:/BtSoft/panel/plugin/total_nginx",
        "compile_args": 0,
        "version_coexist": 0,
        "icon": "icon",
        "preview_url": "",
        "ex1": "",
        "ex2": "",
        "ex3": "",
        "versions": [
          {
            "id": 121,
            "soft_id": 87,
            "m_version": "3",
            "version": "0",
            "dependnet": "",
            "mem_limit": 32,
            "cpu_limit": 1,
            "update_msg": "",
            "os_limit": 0,
            "beta": 0,
            "update_time": 1627458092
          }
        ],
        "endtime": -1,
        "index_display": false,
        "setup": false,
        "status": false,
        "task": "1",
        "is_beta": false
      }
    ]
  }
}
```
