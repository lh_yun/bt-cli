'use strict';
const qs = require("qs");
const axios = require("axios");
const CryptoJS = require("crypto-js");
const baota = {
    host: '127.0.0.1',//地址
    port: 8888,//端口
    key: '',//密钥
    https: false,//使用 https
    /**
     * 发送请求
     * @param url 地址
     * @param data 数据
     * @returns {AxiosPromise}
     */
    request(url, data) {
        let config = {
            baseURL: (this.https ? 'https' : 'http') + '://' + this.host + ':' + this.port,
            params: this.getKyeData(),
            method: 'post',
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            data: qs.stringify({
                'data': data
            })
        }
        return axios(config)
    },
    /**
     * 获取密钥参数
     * @returns {{request_time: number, request_token: string}}
     */
    getKyeData() {
        let now = Date.now()
        let request_token = CryptoJS.MD5(now + CryptoJS.MD5(this.key))
        return {
            "request_time": now,
            "request_token": request_token.toString()
        }
    },
    /**
     * 项目
     */
    project: {
        java: {
            /**
             * JAVA项目列表
             * @param search 项目名称
             * @param p 页数
             * @param limit 行数
             * @returns {*}
             */
            getProjectList(search, p = 1, limit = 10) {
                let url = '/project/java/get_project_list'
                let data = {"p": p, "limit": limit, "search": search}
                return baota.request(url, JSON.stringify(data))
            },
            /**
             *  JAVA启动项目
             * @param projectName 项目名称
             * @returns {*}
             */
            startProject(projectName) {
                let url = '/project/java/start_project'
                let data = {"project_name": projectName}
                return baota.request(url, JSON.stringify(data))
            },
            /**
             * JAVA停止项目
             * @param projectName 项目名称
             * @returns {*}
             */
            stopProject(projectName) {
                let url = '/project/java/stop_project'
                let data = {"project_name": projectName}
                return baota.request(url, JSON.stringify(data))
            }
        }
    }
};
module.exports = baota;