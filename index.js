#!/usr/bin/env node
'use strict';
const program = require('commander')
let baota = require('./baota')

/**
 * 全局参数，自定义请求
 */
program
    .name('bt-cli')
    .description('宝塔CLI node Api工具，必须在开启API接口和配置IP白名单，Api的配置：地址、端口、密钥。' +
        '\n宝塔API接口使用教程 https://www.bt.cn/bbs/thread-20376-1-1.html')
    .version('v' + require('./package.json').version, '-V, --version', '输出版本号')
    .option('-h, --host <host>', '宝塔地址', baota.host)
    .option('-P, --port <number>', '宝塔端口号', baota.port)
    .option('--https', '使用 https', baota.https)
    .requiredOption('-k, --key <key>', '宝塔密钥,必填')
    .argument('<url>', 'URL接口,值 /system?action=GetSystemTotal，例 bt-cli -k 接口密钥 /system?action=GetSystemTotal')
    .argument('[data]', 'URL参数,有特殊符号需要用双引号包裹""，例 "&"')
    .action((url, data) => {
        Object.assign(baota, program.opts())
        if (url) {
            baota.request(url, data).then(function (response) {
                console.log(JSON.stringify(response.data, null, 2))
            }).catch(function (error) {
                console.log(error)
            })
        }
    });
/**
 * java 项目，通过program.opts() 获取全局参数
 */
program.command('java')
    .description('JAVA 项目的列表(list)、启动(start)、停止(stop)')
    .option('-L, --list [projectName]', '项目列表,默认查询10条,例 bt-cli -k 接口密钥 java -L')
    .option('-S, --start <projectName>', '启动项目,例 bt-cli -k 接口密钥 java -S')
    .option('-K, --stop <projectName>', '停止项目,例 bt-cli -k 接口密钥 java -K')
    .action((options) => {
        Object.assign(baota, program.opts())
        if (options.list) {
            baota.project.java.getProjectList(options.list === true ? '' : options.list).then(function (response) {
                console.log(JSON.stringify(response.data, null, 2))
            }).catch(function (error) {
                console.log(error)
            })
        } else if (options.start) {
            baota.project.java.startProject(options.start).then(function (response) {
                console.log(JSON.stringify(response.data))
            }).catch(function (error) {
                console.log(error)
            })
        } else if (options.stop) {
            baota.project.java.stopProject(options.stop).then(function (response) {
                console.log(JSON.stringify(response.data))
            }).catch(function (error) {
                console.log(error)
            })
        }
    });

program.parse();